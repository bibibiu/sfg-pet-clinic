package com.bibibiu.sfgpetclinic.bootstrap;

import com.bibibiu.sfgpetclinic.models.*;
import com.bibibiu.sfgpetclinic.services.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Slf4j
@Component
@SuppressWarnings("UnqualifiedStaticUsage")
public class DataLoader implements CommandLineRunner {

    private final OwnerService ownerService;
    private final PetTypeService petTypeService;
    private final SpecialityService specialityService;
    private final VetService vetService;
    private final VisitService visitService;

    @Autowired
    public DataLoader(OwnerService ownerService, VetService vetService,
                      PetTypeService petTypeService, SpecialityService specialityService, VisitService visitService) {
        this.ownerService = ownerService;
        this.vetService = vetService;
        this.petTypeService = petTypeService;
        this.specialityService = specialityService;
        this.visitService = visitService;
    }

    @Override
    public void run(String... args) {
        int count = petTypeService.findAll().size();
        if (count == 0) {
            loadData();
        }
    }

    private void loadData() {
        log.info("Loading data");

        Speciality radiology = new Speciality("Radiology");
        Speciality savedRadiology = specialityService.save(radiology);

        Speciality surgery = new Speciality("Surgery");
        Speciality savedSurgery = specialityService.save(surgery);

        Speciality dentistry = new Speciality("Dentistry");
        Speciality savedDentistry = specialityService.save(dentistry);

        PetType dog = new PetType("Dog");
        PetType savedDogPetType = petTypeService.save(dog);

        PetType cat = new PetType("Cat");
        PetType savedCatPetType = petTypeService.save(cat);

        Owner tempOwner;
        Visit visit = new Visit();
        Vet tempVet = new Vet();

        tempOwner = Owner.builder()
                .firstName("Michel")
                .lastName("Weston")
                .address("123 Brickerel")
                .city("Miami")
                .telephone("12341234")
                .build();
        Pet mikesPet1 = new Pet("Rosco", savedDogPetType, tempOwner, LocalDate.now());
        tempOwner.addPet(mikesPet1);
        ownerService.save(tempOwner);

        tempOwner = Owner.builder()
                .firstName("Fiona")
                .lastName("Glenanne")
                .address("123 Brickerel")
                .city("Miami")
                .telephone("12341234")
                .build();

        Pet fionaCat = new Pet("Fluffy", savedCatPetType, tempOwner, LocalDate.now());
        tempOwner.addPet(fionaCat);
        ownerService.save(tempOwner);
        log.info("Loaded Owners");

        visit.setPet(fionaCat);
        visit.setDate(LocalDate.now());
        visit.setDescription("Sneezy Cat");
        visitService.save(visit);
        log.info("Loaded Visits");

        tempVet.setId(1L);
        tempVet.setFirstName("Sam");
        tempVet.setLastName("axe");
        tempVet.addSpeciality(savedDentistry);
        vetService.save(tempVet);

        tempVet = new Vet();
        tempVet.setId(2L);
        tempVet.setFirstName("Jessie");
        tempVet.setLastName("Porter");
        tempVet.addSpeciality(savedRadiology);
        vetService.save(tempVet);

        tempVet = new Vet();
        tempVet.setFirstName("Arthur");
        tempVet.setLastName("Mita");
        tempVet.addSpeciality(savedSurgery);
        vetService.save(tempVet);
        log.info("Loaded Vets");


        log.info("Getting Fiona Pets");
        tempOwner.getPets().forEach(pet ->
                System.out.println(pet.getName() + " " + pet.getPetType().getName()));

        log.info("Getting vets");
        vetService.findAll().forEach(vet -> {
            System.out.println(vet.getFirstName());
            vet.getSpecialities().forEach(speciality ->
                    System.out.println("Speciality: " + speciality.getDescription()));
        });

        log.info("Getting visits");
        visitService.findAll().forEach(visit1 -> {
            System.out.println(visit1.getDate());
            System.out.println(visit1.getDescription());
            System.out.println(visit1.getPet().getName());
            System.out.println(visit1.getPet().getOwner().getFirstName());
        });

        log.info("Getting owner using last name");
        Owner weston = ownerService.findByLastName("Weston");
        String ownerDetails = String.format("FirstName: %s\nLastName: %s\nCity: %s\n",
                weston.getFirstName(), weston.getLastName(), weston.getCity());
        System.out.println(ownerDetails);
    }
}
