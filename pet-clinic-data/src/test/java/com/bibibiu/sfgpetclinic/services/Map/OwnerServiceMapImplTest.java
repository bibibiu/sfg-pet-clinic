package com.bibibiu.sfgpetclinic.services.Map;

import com.bibibiu.sfgpetclinic.models.Owner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class OwnerServiceMapImplTest {

    private OwnerServiceMapImpl ownerServiceMap;
    private Long ownerId = 1L;


    @BeforeEach
    void setUp() {
        ownerServiceMap = new OwnerServiceMapImpl(new PetServiceMapImpl(), new PetTypeServiceMapImpl());
        Owner owner = Owner.builder().firstName("Arthur").lastName("Mita").build();
        owner.setPets(new HashSet<>());
        owner.setId(ownerId);
        ownerServiceMap.save(owner);
    }

    @Test
    void findAll() {
        //When
        Set<Owner> owners = ownerServiceMap.findAll();
        assertEquals(1,owners.size());
    }

    @Test
    void deleteById() {
        int count = ownerServiceMap.findAll().size();
        ownerServiceMap.deleteById(ownerId);
        assertEquals(count-1, ownerServiceMap.findAll().size());
    }

    @Test
    void delete() {
        int count = ownerServiceMap.findAll().size();
        ownerServiceMap.delete(ownerServiceMap.findById(ownerId));
        assertEquals(count-1, ownerServiceMap.findAll().size());
    }

    @Test
    void findById() {
        Owner owner = ownerServiceMap.findById(ownerId);
        assertEquals(ownerId, owner.getId());
        assertEquals("Arthur", owner.getFirstName());
        assertEquals("Mita", owner.getLastName());
    }

    @Test
    void findByLastName() {
        String lastName = "Mita";
        Owner owner = ownerServiceMap.findByLastName(lastName);
        assertNotNull(owner);
        assertEquals(lastName, owner.getLastName());
    }

    @Test
    void saveWithId() {
        Long id = 2L;
        Owner owner = Owner.builder().build();
        owner.setId(id);
        Owner savedOwner = ownerServiceMap.save(owner);

        assertEquals(id, savedOwner.getId());

    }

    @Test
    void saveWithoutId() {
        Owner savedOwner = ownerServiceMap.save(Owner.builder().build());
        assertNotNull(savedOwner);
        assertNotNull(savedOwner.getId());
    }
}