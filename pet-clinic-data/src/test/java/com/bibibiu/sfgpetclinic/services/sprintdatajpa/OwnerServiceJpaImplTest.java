package com.bibibiu.sfgpetclinic.services.sprintdatajpa;

import com.bibibiu.sfgpetclinic.models.Owner;
import com.bibibiu.sfgpetclinic.repositories.OwnerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class OwnerServiceJpaImplTest {

    private static final String LAST_NAME = "Mita";

    OwnerServiceJpaImpl ownerServiceJpa;
    @Mock
    OwnerRepository ownerRepository;

    Owner mockedOwner;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        ownerServiceJpa = new OwnerServiceJpaImpl(ownerRepository);
        mockedOwner = Owner.builder().id(1L).firstName("Arthur").lastName(LAST_NAME).build();
    }

    @Test
    void findAll() {

        //Given
        List<Owner> returnedOwners = new ArrayList<>();
        returnedOwners.add(Owner.builder().id(1L).address("00515").build());
        returnedOwners.add(Owner.builder().id(2L).address("00525").build());

        when(ownerRepository.findAll()).thenReturn(returnedOwners);
        Set<Owner> owners = ownerServiceJpa.findAll();

        assertNotNull(owners);
        assertEquals(2, owners.size());
        verify(ownerRepository).findAll();
    }

    @Test
    void findById() {
        //Given
        Long id = 1L;
        when(ownerRepository.findById(anyLong())).thenReturn(Optional.of(mockedOwner));
        Owner owner = ownerServiceJpa.findById(id);
        assertNotNull(owner);
        assertEquals("Arthur",owner.getFirstName());
        verify(ownerRepository).findById(anyLong());

    }

    @Test
    void save() {
        Owner ownerToSave = Owner.builder().id(1L).build();
        when(ownerRepository.save(any())).thenReturn(mockedOwner);
        Owner savedOwner = ownerServiceJpa.save(ownerToSave);
        System.out.println(savedOwner.getLastName());
        assertNotNull(savedOwner);
        verify(ownerRepository).save(ownerToSave);
    }

    @Test
    void delete() {
        ownerServiceJpa.delete(mockedOwner);
        verify(ownerRepository).delete(any());
    }

    @Test
    void deleteById() {
        Long id = 1L;
        ownerServiceJpa.deleteById(id);
        verify(ownerRepository).deleteById(anyLong());
    }


    @Test
    void findByLastName() {
        assertNotNull(ownerServiceJpa);
        //Given
        when(ownerRepository.findByLastName(LAST_NAME)).thenReturn(Optional.ofNullable(mockedOwner));
        Owner returnedOwner = ownerServiceJpa.findByLastName(LAST_NAME);

        assertNotNull(returnedOwner);
        assertEquals(LAST_NAME, returnedOwner.getLastName(), "Din't get entity");
        verify(ownerRepository).findByLastName(anyString());
    }
}