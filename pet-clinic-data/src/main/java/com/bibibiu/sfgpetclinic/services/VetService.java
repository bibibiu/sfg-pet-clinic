package com.bibibiu.sfgpetclinic.services;

import com.bibibiu.sfgpetclinic.models.Vet;

public interface VetService extends CrudService<Vet, Long> {
}
