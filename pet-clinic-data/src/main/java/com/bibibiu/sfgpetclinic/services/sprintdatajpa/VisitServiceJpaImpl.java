package com.bibibiu.sfgpetclinic.services.sprintdatajpa;

import com.bibibiu.sfgpetclinic.models.Visit;
import com.bibibiu.sfgpetclinic.repositories.VisitRepository;
import com.bibibiu.sfgpetclinic.services.VisitService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
@Profile("jpa")
public class VisitServiceJpaImpl implements VisitService {
    private final VisitRepository visitRepository;

    public VisitServiceJpaImpl(VisitRepository visitRepository) {
        this.visitRepository = visitRepository;
    }

    @Override
    public Set<Visit> findAll() {
        return new HashSet<>(visitRepository.findAll());
    }

    @Override
    public Visit findById(Long id) {
        Optional<Visit> visitOptional = visitRepository.findById(id);
        return visitOptional.orElseThrow(()->
                new RuntimeException("Visit with id-->"+ "<--doesn't exist"));
    }

    @Override
    public Visit save(Visit object) {
        return visitRepository.save(object);
    }

    @Override
    public void delete(Visit object) {
        visitRepository.delete(object);
    }

    @Override
    public void deleteById(Long id) {
        visitRepository.deleteById(id);
    }
}
