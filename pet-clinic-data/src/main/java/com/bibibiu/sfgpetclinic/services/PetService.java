package com.bibibiu.sfgpetclinic.services;

import com.bibibiu.sfgpetclinic.models.Pet;

public interface PetService extends CrudService<Pet,Long> {
}
