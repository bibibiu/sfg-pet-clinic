package com.bibibiu.sfgpetclinic.services.Map;

import com.bibibiu.sfgpetclinic.models.Visit;
import com.bibibiu.sfgpetclinic.services.VisitService;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@Profile({"default","map"})
public class VisitMapServiceMapImpl extends AbstractMapService<Visit,Long> implements VisitService {
    @Override
    public Set<Visit> findAll() {
        return super.findAll();
    }

    @Override
    public Visit findById(Long id) {
        return super.findById(id);
    }

    @Override
    public Visit save(@NotNull Visit object) {
        if (isSaveVisitValidation(object)) throw new RuntimeException("Invalid Vist");
        return super.save(object);
    }

    @SuppressWarnings("MethodMayBeStatic")
    private boolean isSaveVisitValidation(@NotNull Visit object) {
        return object.getPet()==null || object.getPet().getOwner()==null
                || object.getPet().getId()==null || object.getPet().getOwner().getId()==null;
    }

    @Override
    public void delete(Visit object) {
        super.delete(object);
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }
}
