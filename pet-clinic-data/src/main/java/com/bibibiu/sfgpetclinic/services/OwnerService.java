package com.bibibiu.sfgpetclinic.services;

import com.bibibiu.sfgpetclinic.models.Owner;

public interface OwnerService extends CrudService<Owner, Long>{
    Owner findByLastName(String lastName);
}
