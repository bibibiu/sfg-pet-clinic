package com.bibibiu.sfgpetclinic.services;

import com.bibibiu.sfgpetclinic.models.Speciality;

public interface SpecialityService extends CrudService<Speciality,Long> {
}
