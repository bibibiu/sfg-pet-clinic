package com.bibibiu.sfgpetclinic.services.Map;

import com.bibibiu.sfgpetclinic.models.Speciality;
import com.bibibiu.sfgpetclinic.models.Vet;
import com.bibibiu.sfgpetclinic.services.SpecialityService;
import com.bibibiu.sfgpetclinic.services.VetService;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
@Profile({"default","map"})
public class VetServiceMapImpl extends AbstractMapService<Vet,Long> implements VetService {

    private final SpecialityService specialityService;

    public VetServiceMapImpl(SpecialityService specialityService) {
        this.specialityService = specialityService;
    }

    @Override
    public Set<Vet> findAll() {
        return super.findAll();
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(Vet object) {
        super.delete(object);
    }

    @Override
    public Vet save(@NotNull Vet object) {
        if (!object.getSpecialities().isEmpty()) {
            object.getSpecialities().forEach(speciality-> {
                Optional<Long> result = Optional.ofNullable(speciality.getId());
                result.orElseGet(() -> {
                    Speciality saveSpecialty = specialityService.save(speciality);
                    Long petId = saveSpecialty.getId();
                    speciality.setId(petId);
                    return petId;
                });
            });
        }
        return super.save(object);
    }

    @Override
    public Vet findById(Long id) {
        return super.findById(id);
    }
}
