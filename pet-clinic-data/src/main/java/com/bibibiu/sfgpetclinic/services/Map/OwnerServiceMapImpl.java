package com.bibibiu.sfgpetclinic.services.Map;

import com.bibibiu.sfgpetclinic.models.Owner;
import com.bibibiu.sfgpetclinic.models.Pet;
import com.bibibiu.sfgpetclinic.models.PetType;
import com.bibibiu.sfgpetclinic.services.OwnerService;
import com.bibibiu.sfgpetclinic.services.PetService;
import com.bibibiu.sfgpetclinic.services.PetTypeService;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
@Profile({"default","map"})
public class OwnerServiceMapImpl extends AbstractMapService<Owner, Long> implements OwnerService {

    private final PetService petService;
    private final PetTypeService petTypeService;

    @Autowired
    public OwnerServiceMapImpl(PetService petService, PetTypeService petTypeService) {
        this.petService = petService;
        this.petTypeService = petTypeService;
    }

    @Override
    public Set<Owner> findAll() {
        return super.findAll();
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(Owner object) {
        super.delete(object);
    }

    @Override
    public Owner findById(Long id) {
        return super.findById(id);
    }

    @Override
    public Owner findByLastName(@NonNls String lastName) {
        /*Set<Owner> owners = findAll();
        for (Owner owner: owners
             ) {
            if (owner.getLastName().equals(lastName)) {
                return owner;
            }
        }*/
        return this
                .findAll()
                .stream()
                .filter(owner -> owner.getLastName().equals(lastName))
                .findFirst()
                .orElseThrow(()->new RuntimeException("Owner with last name-->"+ lastName +"<-- does't exist"));
    }

    @Override
    public Owner save(@NotNull Owner object) {
        if (object.getPets() != null &&!object.getPets().isEmpty()) {
            object.getPets().forEach(pet -> {
                //Checks if PetTypeRepository is not null else throws runtime exception
                Optional<PetType> result = Optional.ofNullable(pet.getPetType());
                //if not null save it and addPet PetTypeRepository to owner
                PetType petType = result.orElseThrow(() -> {
                    throw new RuntimeException("PetTypeRepository is required");
                });
                pet.setPetType(petTypeService.save(petType));

                //checks whether pet id is null
                Optional<Long> result1 = Optional.ofNullable(pet.getId());
                result1.orElseGet(() -> {
                    Pet savedPet = petService.save(pet);
                    Long petId = savedPet.getId();
                    pet.setId(petId);
                    return petId;
                });
            });
        }
        return super.save(object);
    }
}
