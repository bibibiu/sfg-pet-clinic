package com.bibibiu.sfgpetclinic.services.sprintdatajpa;

import com.bibibiu.sfgpetclinic.models.Pet;
import com.bibibiu.sfgpetclinic.repositories.PetRepository;
import com.bibibiu.sfgpetclinic.services.PetService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
@Profile("jpa")
public class PetServiceJpaImpl implements PetService {

    private final PetRepository petRepository;

    public PetServiceJpaImpl(PetRepository petRepository) {
        this.petRepository = petRepository;
    }

    @Override
    public Set<Pet> findAll() {
        return new HashSet<>(petRepository.findAll());
    }

    @Override
    public Pet findById(Long id) {
        Optional<Pet> optionalPet = petRepository.findById(id);
        return optionalPet.orElseThrow(()->
                new RuntimeException("Pet id-->"+ id +"<--doesn't exist"));
    }

    @Override
    public Pet save(Pet object) {
        return petRepository.save(object);
    }

    @Override
    public void delete(Pet object) {
        petRepository.delete(object);
    }

    @Override
    public void deleteById(Long id) {
        petRepository.deleteById(id);
    }
}
