package com.bibibiu.sfgpetclinic.services;

import com.bibibiu.sfgpetclinic.models.PetType;

public interface PetTypeService extends CrudService<PetType,Long> {
}
