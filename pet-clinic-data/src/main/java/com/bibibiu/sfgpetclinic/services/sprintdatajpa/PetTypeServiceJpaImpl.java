package com.bibibiu.sfgpetclinic.services.sprintdatajpa;

import com.bibibiu.sfgpetclinic.models.PetType;
import com.bibibiu.sfgpetclinic.repositories.PetTypeRepository;
import com.bibibiu.sfgpetclinic.services.PetTypeService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
@Profile("jpa")
public class PetTypeServiceJpaImpl implements PetTypeService {
    private final PetTypeRepository petTypeRepository;

    public PetTypeServiceJpaImpl(PetTypeRepository petTypeRepository) {
        this.petTypeRepository = petTypeRepository;
    }

    @Override
    public Set<PetType> findAll() {
        return new HashSet<>(petTypeRepository.findAll());
    }

    @Override
    public PetType findById(Long id) {
        Optional<PetType> optionalPetType = petTypeRepository.findById(id);
        return optionalPetType.orElseThrow(()->
                new RuntimeException("PetType id-->"+ id + "<--not found"));
    }

    @Override
    public PetType save(PetType object) {
        return petTypeRepository.save(object);
    }

    @Override
    public void delete(PetType object) {
        petTypeRepository.delete(object);
    }

    @Override
    public void deleteById(Long id) {
        petTypeRepository.deleteById(id);
    }
}
