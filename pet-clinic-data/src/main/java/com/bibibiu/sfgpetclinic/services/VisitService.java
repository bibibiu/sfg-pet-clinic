package com.bibibiu.sfgpetclinic.services;

import com.bibibiu.sfgpetclinic.models.Visit;

public interface VisitService extends CrudService<Visit,Long> {
}
