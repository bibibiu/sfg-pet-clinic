package com.bibibiu.sfgpetclinic.services.Map;

import com.bibibiu.sfgpetclinic.models.BaseEntity;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public abstract class AbstractMapService<T extends BaseEntity, ID extends Long> {
    protected Map<Long, T> map = new HashMap<>();
    Set<T> findAll(){
        return new HashSet<>(map.values());
    }
    T save(@NotNull T object) {
        if (object == null) {
            throw new RuntimeException("Object is null");
        }
        Optional<Long> result = Optional.ofNullable(object.getId());
        if (!result.isPresent()) {
             object.setId(nextId());
        }
        map.put(object.getId(), object);
        return object;
    }
    T findById(ID id){
        return map.get(id);
    }
    void deleteById (ID id){
        map.remove(id);
    }

    void delete (T object) {
        map.entrySet().removeIf(idtEntry -> idtEntry.getValue().equals(object));
    }

    private Long nextId() {
        long nextId;
        try {
            nextId = Collections.max(map.keySet()) + 1L;
        } catch (NoSuchElementException e) {
            nextId = 1L;
        }
        return nextId;
    }

}
