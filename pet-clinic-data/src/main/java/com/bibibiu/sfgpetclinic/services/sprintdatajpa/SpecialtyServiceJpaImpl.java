package com.bibibiu.sfgpetclinic.services.sprintdatajpa;

import com.bibibiu.sfgpetclinic.models.Speciality;
import com.bibibiu.sfgpetclinic.repositories.SpecialtyRepository;
import com.bibibiu.sfgpetclinic.services.SpecialityService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
@Profile("jpa")
public class SpecialtyServiceJpaImpl implements SpecialityService {
    private final SpecialtyRepository specialtyRepository;

    public SpecialtyServiceJpaImpl(SpecialtyRepository specialtyRepository) {
        this.specialtyRepository = specialtyRepository;
    }

    @Override
    public Set<Speciality> findAll() {
        return new HashSet<>(specialtyRepository.findAll());
    }

    @Override
    public Speciality findById(Long id) {
        Optional<Speciality> specialityOptional = specialtyRepository.findById(id);
        return specialityOptional.orElseThrow(()->
                new RuntimeException("Speciality id-->"+ id +" doesn't exist"));
    }

    @Override
    public Speciality save(Speciality object) {
        return specialtyRepository.save(object);
    }

    @Override
    public void delete(Speciality object) {
        specialtyRepository.delete(object);
    }

    @Override
    public void deleteById(Long id) {
        specialtyRepository.deleteById(id);
    }
}
