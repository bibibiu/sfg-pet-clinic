package com.bibibiu.sfgpetclinic.services.sprintdatajpa;

import com.bibibiu.sfgpetclinic.models.Vet;
import com.bibibiu.sfgpetclinic.repositories.VetRepository;
import com.bibibiu.sfgpetclinic.services.VetService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
@Profile("jpa")
public class VetServiceJpaImpl implements VetService {
    private final VetRepository vetRepository;

    public VetServiceJpaImpl(VetRepository vetRepository) {
        this.vetRepository = vetRepository;
    }

    @Override
    public Set<Vet> findAll() {
        return new HashSet<>(vetRepository.findAll());
    }

    @Override
    public Vet findById(Long id) {
        Optional<Vet> optionalVet = vetRepository.findById(id);
        return optionalVet.orElseThrow(() ->
                new RuntimeException("Vet id-->"+ id +"<--doesn't exist"));
    }

    @Override
    public Vet save(Vet object) {
        return vetRepository.save(object);
    }

    @Override
    public void delete(Vet object) {
        vetRepository.delete(object);
    }

    @Override
    public void deleteById(Long id) {
        vetRepository.deleteById(id);
    }
}
