package com.bibibiu.sfgpetclinic.repositories;

import com.bibibiu.sfgpetclinic.models.Speciality;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpecialtyRepository extends JpaRepository<Speciality,Long> {
}
