package com.bibibiu.sfgpetclinic.repositories;

import com.bibibiu.sfgpetclinic.models.Owner;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OwnerRepository extends JpaRepository<Owner,Long> {
    Optional<Owner> findByLastName(String lastName);
}
