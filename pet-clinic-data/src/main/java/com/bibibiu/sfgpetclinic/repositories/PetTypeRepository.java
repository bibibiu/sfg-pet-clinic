package com.bibibiu.sfgpetclinic.repositories;

import com.bibibiu.sfgpetclinic.models.PetType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetTypeRepository extends JpaRepository<PetType,Long> {
}
