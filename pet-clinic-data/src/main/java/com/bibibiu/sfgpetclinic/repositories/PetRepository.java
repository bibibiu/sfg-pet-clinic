package com.bibibiu.sfgpetclinic.repositories;

import com.bibibiu.sfgpetclinic.models.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetRepository extends JpaRepository<Pet,Long> {
}
