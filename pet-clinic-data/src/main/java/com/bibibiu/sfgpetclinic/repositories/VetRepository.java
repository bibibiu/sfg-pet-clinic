package com.bibibiu.sfgpetclinic.repositories;

import com.bibibiu.sfgpetclinic.models.Vet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VetRepository extends JpaRepository<Vet, Long> {
}
