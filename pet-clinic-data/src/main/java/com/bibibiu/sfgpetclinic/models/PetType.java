package com.bibibiu.sfgpetclinic.models;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@NoArgsConstructor
@Table(name = "types")
@EqualsAndHashCode(callSuper = false)
public class PetType extends NamedEntity {
    @Builder
    public PetType(String name) {
        super(name);
    }
}
