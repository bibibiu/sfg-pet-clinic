package com.bibibiu.sfgpetclinic.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Data
@Entity
@Table(name = "vets")
@EqualsAndHashCode(callSuper = false)
public class Vet extends Person{

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "vet_specialities",
            joinColumns = @JoinColumn(name = "vet_id"),
            inverseJoinColumns = @JoinColumn(name = "specialty_id"))
    private Set<Speciality> specialities;

    /**
     * Convenience method for adding specialities
     * @param speciality speciality to be added
     */
    public void addSpeciality(Speciality speciality) {
        Optional<Set<Speciality>> result = Optional.ofNullable(this.specialities);
        this.specialities = result.orElseGet(HashSet::new);
        this.specialities.add(speciality);
    }
}
