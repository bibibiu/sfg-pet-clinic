package com.bibibiu.sfgpetclinic.models;

import lombok.*;
import org.jetbrains.annotations.NotNull;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@Table(name = "owners")
/*@ToString(callSuper = true)*/
@EqualsAndHashCode(callSuper = true,exclude = "pets")
public class Owner extends Person {
    private @NonNull String address;
    private @NonNull String city;
    private @NonNull String telephone;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "owner",orphanRemoval = true)
    private Set<Pet> pets;

    @Builder
    public Owner(Long id, String firstName, String lastName, String address, String city, String telephone) {
        super(firstName,lastName);
        this.address = address;
        this.city = city;
        this.telephone = telephone;
    }

    public void addPet(@NotNull Pet pet) {
        Optional<Set<Pet>> result = Optional.ofNullable(this.pets);
        this.pets = result.orElseGet(HashSet::new);
        this.pets.add(pet);
    }
}
