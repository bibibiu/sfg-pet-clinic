package com.bibibiu.sfgpetclinic.models;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@Table(name = "pets")
@EqualsAndHashCode(callSuper = false, exclude = {"owner","visits"})
public class Pet extends NamedEntity{
    private LocalDate birthday;
    @ManyToOne
    @JoinColumn(name = "owner_id")
    private Owner owner;
    @ManyToOne
    @JoinColumn(name = "type_id")
    private PetType petType;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pet")
    private Set<Visit> visits;

    @Builder
    public Pet(String name, PetType petType, Owner owner, LocalDate birthday) {
        super(name);
        this.petType = petType;
        this.owner = owner;
        this.birthday = birthday;
    }
}
