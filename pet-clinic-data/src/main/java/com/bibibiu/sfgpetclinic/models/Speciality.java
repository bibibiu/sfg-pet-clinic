package com.bibibiu.sfgpetclinic.models;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "specialities")
@EqualsAndHashCode(callSuper = false)
@Builder
public class Speciality extends BaseEntity {
    private String Description;
}
